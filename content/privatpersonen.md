---
title: "Privatpersonen"
title-meta: "Privatpersonen"
image-variant: 1
---

::: content

# Beratung und Rechtsvertretung

Brauchen Sie Informationen im Sozialversicherungsbereich oder zum Arbeitsrecht?
Möchten Sie, dass ich Ihren laufenden Fall übernehme oder Sie begleite?
Ich unterstütze Sie je nach Ihren Bedürfnissen, beratend oder in Rechtsvertretung.

# Themen und Rechtsgebiete

- Arbeitsunfähigkeit infolge Krankheit oder Unfall
- Chronische körperliche und/oder psychische Erkrankungen
- Invalidität
- Berufliche Vorsorge, Frühpensionierung
- Geburtsgebrechen
- Koordination der Leistungsansprüche bei den verschiedenen Sozialversicherungen
- Arbeitsrecht

# Einige Beispiele aus der Praxis

- Arbeitsrechtliche Streitigkeiten, in Kombination mit Arbeitsunfähigkeit (Kündigung, Mobbing)
- Einsprachen gegenüber Invaliden- oder Unfallversicherung
- Geltendmachung von Kranken- oder Unfalltaggeld, von Leistungen gegenüber der beruflichen Vorsorge sowie von Arbeitslosenentschädigung bei Arbeitsunfähigkeit
- Aushandeln von sozialverträglichen Auflösungsvereinbarungen mit den Arbeitgebern

Coaching sowie konstruktive Lösungsvorschläge bei der beruflichen Wiedereingliederung oder Mediation bei Arbeitsstreitigkeiten runden mein Angebot ab.

:::

::: highlight

# Ihr Nutzen

Sie sparen Zeit, Energie und Geld. In einem persönlichen Erstgespräch machen wir gemeinsam eine Standortbestimmung. Dabei entscheiden Sie, in welchem Umfang ich die Fallführung übernehmen soll. Sie können mir Ihren ganzen administrativen Aktenberg übergeben oder nur Teile davon.

Mögliches Vorgehen: Vollmachtserteilung, Akteneinsicht, Fallevaluation, Koordination, Geltendmachung Ihrer Leistungsansprüche.

:::
