---
title: "Myrna Afèche"
notice:
  active: false
  text: "Trauma und Recht | Donnerstag 14. November 2019 | Tri-Regio-Netzwerk für Psychotraumatologie"
  file: "/files/SVBT-14-Nov-2019.pdf"
---

::: content

Im komplexen rechtlichen, medizinischen und sozialen Netz steht für mich der Mensch im Vordergrund.
Ich biete Ihnen eine ganzheitliche Betrachtung Ihres Falls.
Für die Lösung setze ich mein fundiertes Fachwissen sowie meine breitgefächerte, grosse Erfahrung ein.

Meine Dienstleistungen richten sich an Privatpersonen, Firmen und medizinische Fachleute, die ich verantwortungsvoll und mit höchster Sorgfalt berate und vertrete.

:::
