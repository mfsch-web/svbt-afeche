---
title: "Firmen"
title-meta: "Firmen"
image-variant: 3
image-right: true
---

::: {.content .compact}

Krankheit, Unfälle, Invalidität, Frühpensionierungen – sozialversicherungsrechtliche Fragen sind komplex.
Ich unterstütze Ihre Personal- und Sozialdienste mit Fachwissen und Erfahrung.
Auch bei herausfordernden Problemstellungen finde ich effektive Lösungen.

# Fachberatung

Meine Fachkompetenz im gesamten Sozialversicherungsbereich inklusive Arbeitsrecht steht Ihnen zur Verfügung.
Bei eingeschränkter oder gänzlich entfallener Arbeitsleistung von Mitarbeitenden finde ich Wege aus der Abwärtsspirale und wahre Ihre finanziellen Interessen.

# Rechtsvertretung

Ich mache Ihre Ansprüche gegenüber Dritten geltend.

# Begleitkonzepte

Sowohl bei der Koordination und Geltendmachung verschiedener Leistungsansprüche als auch bei der Wiedereingliederung Ihrer Mitarbeitenden unterstütze ich Sie – bei Bedarf ergänzend mit Coaching oder Mediation.
Auch eine sozialverträgliche Auflösungsvereinbarung kann Ziel meiner Begleitung sein.

# Weiterbildung

Mit praxisbezogenen Schulungen, die auf Sie zugeschnitten sind, bringe ich Ihre Mitarbeitenden in den Personal- und Sozialdiensten auf den neusten Stand.

:::

::: highlight

# Ihr Nutzen

Dank einer präzisen Standortbestimmung gewinnen Sie den Überblick über die zuständigen Sozialversicherungen, die rechtlich begründeten Leistungsansprüche und deren zeitliche und inhaltliche Abfolge.
Ich prüfe und beschleunige die Verfahrensabläufe bei den Versicherungsträgern.
So entlaste ich Sie, damit Sie sich Ihren Kernaufgaben widmen können.

:::
