---
title: "Fachleute Medizin"
title-meta: "Fachleute Medizin"
image-variant: 2
---

::: content

Stetige Veränderungen und Entwicklungen bei den Sozialversicherungen, deren gesetzlichen Grundlagen und in der Rechtsprechung erfordern zunehmend fachspezifische und administrative Kenntnisse von Ihnen als Ärztin/Arzt.
Die Fragestellungen in der medizinischen Berichterstattung werden immer komplexer und umfangreicher.
Gleichzeitig erwarten die Patientinnen und Patienten eine umfassende Betreuung, die teils weit über das Medizinische hinausgeht.

In diesem Spannungsfeld biete ich Ihnen fachliche Unterstützung und Beratung, massgeschneidert auf Indikationen oder Diagnosen der verschiedenen Krankheiten und Unfallfolgen unter Einbezug der Rechtslage und Rechtsprechung.

# Angebote

- Interdisziplinäre Zusammenarbeit in der Berichterstattung gegenüber den Sozialversicherungen
- Fachveranstaltungen (Weiterbildung, Workshops, Supervision)

# Einige Beispiele aus der Praxis

- Welche sozialversicherungsrechtlichen Auswirkungen hat die attestierte Arbeitsunfähigkeit? 
- Welche Informationen dürfen oder sollen im Arbeitsunfähigkeitszeugnis preisgegeben werden?
- Welche Kriterien und Aussagen sind in Berichten und Arztzeugnissen zuhanden der Sozialversicherungen zu beachten?
- Wie wird das Instrument des Arbeitsversuchs sinnvoll eingesetzt?

:::

::: highlight

# Ihr Nutzen

Mit meinem Fachwissen unterstütze ich Sie bei interdisziplinären Fragestellungen an den Schnittstellen von Medizin–Recht–Sozialversicherungen. Durch das Klären von Abläufen und Strukturen der Sozialversicherungen werden Sie entlastet und können sich auf Ihre medizinische Tätigkeit konzentrieren.

:::
