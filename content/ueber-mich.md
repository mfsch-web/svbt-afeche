---
title: "Über mich"
title-meta: "Über mich & Kontakt"
image-variant: 4
---

::: {.highlight}

# Kontakt

SVBT GmbH SozialversicherungsBeratung  
Myrna Afèche  
lic. iur., Sozialversicherungsfachfrau  
mit eidg. Fachausweis 
<script type="text/javascript">
var a = new Array('bt&#x0002E;ch','afec','he&#x00040;sv');
document.write('<a href="mai'+'lto:'+a[1]+a[2]+a[0]+'">'+a[1]+a[2]+a[0]+'</a>');
</script><noscript><div class="addr-hidden" data-n1="afec" data-n2="he" data-domain="svbt" data-tld="ch"></div></noscript>  
[svbt.ch](https://svbt.ch)  
<!--[Karte](https://map.search.ch/SVBT-GmbH,Basel,Blumenrain-3?z=2048){.map}-->
<!--[Karte](https://www.google.com/maps/search/?api=1&query=SVBT+Basel&query_place_id=ChIJAQBcIa-5kUcRb0H8bKb1egI){.map}-->

<figure class="portrait">
  <img src="/img/portrait.jpg" alt="Porträt Myrna Afèche" />
</figure>

:::

::: content

# Juristin und Sozialversicherungsfachfrau mit eidg. Fachausweis

Es ist mir ein Anliegen, meine Klientinnen und Klienten umfassend zu beraten und zu begleiten.
Sozialversicherungsfälle haben oft eine lange persönliche Vorgeschichte, sind komplex und belastend.
Wenn ich für die Betroffenen einstehe, ihre Rechte und Ansprüche prüfe sowie geltend mache, so führt dies nicht nur zu einer juristischen Lösung, sondern bringt oft auch den persönlichen Verarbeitungsprozess voran und unterstützt einen Neubeginn.
Im Rahmen dieser Begleitung greife ich auf meine langjährigen beruflichen und persönlichen Erfahrungen zurück.

# Ausbildung

- Lizentiat der Rechtswissenschaften, Universität Basel
- Sozialversicherungsfachfrau mit eidg. Fachausweis
- Weiterbildung in lösungsfokussierter Kompetenz für Coaching und Mediation, Ausbildungsinstitut Perspectiva, Basel

# Berufliche Erfahrung

- Juristin und Fachberaterin bei der Procap Nordwestscheiz
- Juristin in verschiedenen Branchen (Bank, Pharma, Verwaltung)
- Selbständig seit 2009

# Lehrtätigkeit

- Mündliches Prüfungstraining für Soziale Sicherheit beim RVK
- Prüfungsexpertin für den eidg. Fachausweis, Schweizerischer Verband der Sozialversicherungs-Fachleute SVS
- Dozentin für die Fächer Soziale Sicherheit, Struktur der Sozialversicherungen und Volkswirtschaftliche Zusammenhänge, eidg. Berufsausbildung SVS, Basel
- Dozentin im Lehrgang Sachbearbeiter/in Sozialversicherungen, KV Basel
- Durchführung diverser Weiterbildungen und Seminare in den Bereichen Medizin, Recht, Sozialarbeit

# Engagements

- Vorstandsmitglied im TriRegio Netzwerk für Psychotraumatologie, seit 2018
- Stiftungsrätin des Wohn- und Bürozentrums für Körperbehinderte WBZ, 2010–2016

:::
