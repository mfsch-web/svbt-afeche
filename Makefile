.DEFAULT_GOAL := site
.PHONY: site clean

SITE = public
TOOLS = tools

content = $(patsubst content/%.md,%.html,$(wildcard content/*.md))

options_plants_wide = -extract '7400x1850+70+970' -flop
options_plants_1 = -extract '4160x2743+130+60'
options_plants_2 = -extract '2880x1899+700+1950'
options_plants_3 = -extract '2240x1477+6460+3430'
options_plants_4 = -extract '4160x2743+150+1550'

site-files = $(content) \
    css/main.css \
    img/portrait.jpg \
    img/plants-1.jpg \
    img/plants-2.jpg \
    img/plants-3.jpg \
    img/plants-4.jpg \
    img/plants-wide.jpg \
    favicon.ico \
    googleeb2488027923d76d.html

$(info site: $(site-files))

site: $(addprefix $(SITE)/,$(site-files))

clean:
	rm -rf $(SITE) $(TOOLS) node_modules

$(TOOLS)/dart-sass/sass:
	mkdir -p $(TOOLS)
	wget -O $(TOOLS)/dart-sass.tar.gz https://github.com/sass/dart-sass/releases/download/1.25.0/dart-sass-1.25.0-linux-x64.tar.gz
	tar -xzf $(TOOLS)/dart-sass.tar.gz -C $(TOOLS)

$(SITE)/img/plants-%.jpg: images/plants.jpg
	mkdir -p $(@D)
	convert +level-colors ,"#41b23b" -auto-level $(options_plants_$*) -resize $(if $(findstring $*,wide),'1280x320','640x422') $< -quality 70 $@

$(SITE)/img/%.jpg: images/%.jpg
	mkdir -p $(@D)
	cp $< $@

$(SITE)/favicon.ico: images/favicon.ico
	mkdir -p $(@D)
	cp $< $@

$(SITE)/google%.html:
	echo "google-site-verification: $(@F)" > $@

$(SITE)/%.html: content/%.md template.html
	mkdir -p $(@D)
	pandoc --template=template.html "--variable=nav-$*" --base-header-level=2 --section-divs --strip-comments "--output=$@" $<

$(SITE)/css/main.css: styles/main.scss styles/*.scss | $(SITE) $(TOOLS)/dart-sass/sass
	mkdir -p $(@D)
	tools/dart-sass/sass $< $@
	npx postcss $@ --replace --use autoprefixer
